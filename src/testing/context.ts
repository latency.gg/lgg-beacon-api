import * as authApiSpec from "@latency.gg/lgg-auth-oas";
import * as beaconApiSpec from "@latency.gg/lgg-beacon-oas";
import * as db from "@latency.gg/lgg-db";
import * as http from "http";
import pg from "pg";
import * as prom from "prom-client";
import { Promisable } from "type-fest";
import * as application from "../application/index.js";

interface Context {
    endpoints: {
        application: URL,
        auth: URL,
        pg: URL,
    },
    servers: {
        application: application.Server,
        auth: authApiSpec.Server,
    },
    services: application.Services,
    createApplicationClient(
        credentials?: beaconApiSpec.Credentials
    ): beaconApiSpec.Client,
}

export async function withContext(
    job: (context: Context) => Promisable<void>,
) {
    const result = withTestDb(
        db.getSchema(),
        new URL(process.env.PGURI ?? "postgres://postgres@localhost:5432/postgres"),
        async pgUri => {
            const endpoints = {
                application: new URL("http://localhost:8080"),
                auth: new URL("http://localhost:9010"),
                pg: pgUri,
            };

            const abortController = new AbortController();
            const promRegistry = new prom.Registry();

            const applicationConfig: application.Config = {
                endpoint: endpoints.application,
                pgUri: endpoints.pg,
                authApiEndpoint: endpoints.auth,

                abortController,
                promRegistry,
            };

            const onError = (error: unknown) => {
                if (!abortController.signal.aborted) {
                    throw error;
                }
            };

            const applicationContext = application.createContext(
                applicationConfig,
            );
            try {

                const servers = {
                    application: application.createServer(
                        applicationContext,
                        onError,
                    ),
                    auth: new authApiSpec.Server({
                        baseUrl: endpoints.auth,
                    }),
                };

                const httpServers = {
                    application: http.createServer(servers.application.asRequestListener({
                        onError,
                    })),
                    auth: http.createServer(servers.auth.asRequestListener({
                        onError,
                    })),
                };

                const context = {
                    endpoints,
                    servers,
                    createApplicationClient(credentials?: beaconApiSpec.Credentials) {
                        return new beaconApiSpec.Client(
                            {
                                baseUrl: endpoints.application,
                            },
                            credentials,
                        );
                    },
                    services: applicationContext.services,
                };

                const keys = Object.keys(httpServers) as Array<keyof typeof httpServers>;
                await Promise.all(
                    keys.map(async key => new Promise<void>(resolve => httpServers[key].listen(
                        endpoints[key].port,
                        () => resolve(),
                    ))),
                );

                try {
                    await job(context);
                }
                finally {
                    abortController.abort();

                    await Promise.all(
                        keys.map(async key => new Promise<void>(
                            (resolve, reject) => httpServers[key].close(
                                error => error ?
                                    reject(error) :
                                    resolve(),
                            )),
                        ),
                    );
                }
            }
            finally {
                await applicationContext.destroy();
            }

        },
    );

    return result;
}

async function withTestDb<T>(
    sql: string,
    pgAdminUri: URL,
    job: (pgUri: URL) => Promisable<T>,
) {
    const dbName = `testdb-${new Date().valueOf()}`;
    const pgUri = new URL(`/${dbName}`, pgAdminUri);

    const pgAdminPool = new pg.Pool({
        connectionString: pgAdminUri.toString(),
    });
    try {
        await pgAdminPool.query(`CREATE DATABASE "${dbName}";`);
        try {
            const pgPool = new pg.Pool({
                connectionString: pgUri.toString(),
            });
            try {
                await pgPool.query(sql);

                const result = await job(pgUri);
                return result;
            }
            finally {
                await pgPool.end();
            }
        }
        finally {
            await pgAdminPool.query(`DROP DATABASE "${dbName}";`);
        }
    }
    finally {
        await pgAdminPool.end();
    }

}
