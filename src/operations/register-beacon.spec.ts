import assert from "assert";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

test("register-beacon", t => withContext(async context => {
    initializeMocks();

    const client = context.createApplicationClient({
        basic: { username: "MQ==", password: "dHJ1ZQ==" },
    });

    const response = await client.registerBeacon({
        parameters: {},
        entity() {
            return {
                ipv4: "0.0.0.0",
                location: "loc1",
                provider: "prov1",
                version: "v0.1.0",
            };
        },
    });
    assert(response.status === 202);

    const responseEntity = await response.entity();
    t.deepEqual(
        responseEntity,
        {
            beacon: responseEntity.beacon,
            ipv4: "0.0.0.0",
            location: "loc1",
            provider: "prov1",
            version: "v0.1.0",
        },
    );

    function initializeMocks() {
        context.servers.auth.registerValidateClientSecretOperation(async incomingRequest => {
            const incomingEntity = await incomingRequest.entity();
            const clientId = incomingRequest.parameters.client;
            const clientSecret = incomingEntity.value;

            return {
                status: 200,
                parameters: {},
                entity() {
                    return { valid: clientId === "MQ==" && clientSecret === "dHJ1ZQ==" };
                },
            };
        });
    }

}));
