import * as authApiSpec from "@latency.gg/lgg-auth-oas";
import { createRedirectMiddleware } from "@oas3/oas3ts-lib";
import pg from "pg";
import { Config } from "./config.js";

export interface Services {
    pgPool: pg.Pool;
    authApi: authApiSpec.Client
}

export function createServices(
    config: Config,
): Services {
    const pgPool = new pg.Pool({
        connectionString: config.pgUri.toString(),
    });

    const authApi = new authApiSpec.Client({
        baseUrl: config.authApiEndpoint,
        httpSendReceive: config.httpSendReceive,
    });
    authApi.registerMiddleware(
        createRedirectMiddleware(),
    );

    return {
        pgPool,
        authApi,
    };
}
