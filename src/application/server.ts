import * as beaconApi from "@latency.gg/lgg-beacon-oas";
import * as oas3ts from "@oas3/oas3ts-lib";
import * as authorization from "../authorization/index.js";
import * as operations from "../operations/index.js";
import { Context } from "./context.js";

export type ServerAuthorization = authorization.BasicAuthorization;

export type Server = beaconApi.Server<ServerAuthorization>;

export function createServer(
    context: Context,
    onError: (error: unknown) => void,
) {
    const server = new beaconApi.Server<ServerAuthorization>({
        baseUrl: context.config.endpoint,
    });

    server.registerMiddleware(oas3ts.createErrorMiddleware(onError));

    server.registerMiddleware(
        async function (this, route, request, next) {
            const operation = route ?
                oas3ts.getOperationId(beaconApi.metadata, route.name, request.method) :
                undefined;
            const stopTimer = context.metrics.operationDuration.startTimer({ operation });
            try {
                const response = await next(request);
                stopTimer({ status: response.status });
                return response;
            }
            catch (error) {
                stopTimer();

                throw error;
            }
        },
    );

    server.registerBasicAuthorization(authorization.createBasicAuthorization(context));

    server.registerSubmitMeasurementsOperation(
        operations.createSubmitMeasurementsOperation(context),
    );
    server.registerRegisterBeaconOperation(
        operations.createRegisterBeaconOperation(context),
    );

    server.registerRegisterNewBeaconOperation(
        operations.createRegisterNewBeaconOperation(context),
    );
    server.registerRecordUdpDataSampleOperation(
        operations.createRecordUdpDataSampleOperation(context),
    );

    return server;
}
