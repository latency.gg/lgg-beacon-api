import * as prom from "prom-client";

export type Metrics = ReturnType<typeof createMetrics>;

export function createMetrics(registry: prom.Registry) {
    const operationDuration = new prom.Histogram({
        name: "operation_duration_seconds",
        help: "time it took to get a response from an operation",
        labelNames: ["operation", "status"],
        buckets: prom.exponentialBuckets(0.002, 2, 16),
        registers: [registry],
    });

    const transactionDuration = new prom.Histogram({
        name: "transaction_duration_seconds",
        help: "time it took to execute a transaction",
        labelNames: ["name", "end"],
        buckets: prom.exponentialBuckets(0.001, 2, 16),
        registers: [registry],
    });

    return {
        operationDuration,
        transactionDuration,
    };
}

