import * as beaconApiSpec from "@latency.gg/lgg-beacon-oas";
import * as application from "../application/index.js";

export interface BasicAuthorization {
    basic: {
        client: Buffer
    }
}

export function createBasicAuthorization(
    context: application.Context,
): beaconApiSpec.BasicAuthorizationHandler<application.ServerAuthorization> {
    return async (
        credential,
    ) => {
        if (credential == null) return;

        const clientId = credential.username;
        const clientSecret = credential.password;
        const clientIdBuffer = Buffer.from(clientId, "base64");

        const validateClientSecretResult = await context.services.authApi.validateClientSecret({
            parameters: {
                client: clientId,
            },
            entity() {
                return { value: clientSecret };
            },
        });

        if (validateClientSecretResult.status !== 200) return;

        const validateClientSecretEntity = await validateClientSecretResult.entity();
        if (!validateClientSecretEntity.valid) return;

        return { client: clientIdBuffer };
    };
}
